# SimpleCalculator #
* Code-dojo for TDD

# AndroidBasicNavigation #
* Basic Navigations
* Layout + Buttons + Action Bars + Menu
* Application Fundamentals: http://developer.android.com/guide/components/fundamentals.html
* Starting an Activity: http://developer.android.com/training/basics/activity-lifecycle/starting.html#Create
* Storage Options: http://developer.android.com/guide/topics/data/data-storage.html
* Creating a Content Provider: http://developer.android.com/guide/topics/providers/content-provider-creating.html
* Customizing Android ListView Items with Custom ArrayAdapter: http://www.ezzylearning.com/tutorial/customizing-android-listview-items-with-custom-arrayadapter
* More info on how to implement PagerTabStrip: 
 * * http://codetheory.in/android-pagertabstrip-pagertitlestrip-viewpager/
 * * http://www.androidbegin.com/tutorial/android-viewpagertabstrip-fragments-tutorial/