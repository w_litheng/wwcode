﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using SimpleCalculator;

namespace TestSimpleCalculator
{
    [TestFixture]
    public class TestSimpleCalculator
    {
        private MySimpleCalculator _calculator = null;

        [SetUp]
        public void Setup()
        {
            _calculator = new MySimpleCalculator(); 
        }
        
        [Test]
        public void TestAddDelimiterComma()
        {
            int actual = 0;
            int expected = 0;
            
            actual = _calculator.Add(String.Empty);
            expected = -1;
            Assert.AreEqual(expected, actual);

            actual = _calculator.Add("1,2");
            expected = 3;
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestAddDelimiterNewlineAndComma()
        {
            int actual = 0;
            int expected = 0;

            actual = _calculator.Add("1,2\n3");
            expected = 6;
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestAddDifferentDelimiters()
        {
            int actual = 0;
            int expected = 0;

            actual = _calculator.Add("//;\n1;2;3");
            expected = 6;
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestAddNegativeNumbers()
        {
            Exception ex = Assert.Throws<Exception>(() => _calculator.Add("//;\n1;-2;3;-4;5"));

            Assert.IsTrue(ex.Message.Contains("-2"));
            Assert.IsTrue(ex.Message.Contains("-4"));
            Assert.IsFalse(ex.Message.Contains("1"));
            Assert.IsFalse(ex.Message.Contains("3"));
            Assert.IsFalse(ex.Message.Contains("5"));
        }

        [Test]
        public void TestIgnore1000()
        {
            int actual = 0;
            int expected = 0;

            actual = _calculator.Add("//;\n1;2;3;1000;1001;4");
            expected = 1010;
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestDelimiterAnyLength()
        {
            int actual = 0;
            int expected = 0;

            actual = _calculator.Add("//;!@\n1;!@2;!@3");
            expected = 6;
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void TestMultipleDelimiter()
        {
            int actual = 0;
            int expected = 0;

            actual = _calculator.Add("//[,][;][@]\n1,2;3@4");
            expected = 10;
            Assert.AreEqual(expected, actual);

        }
    }
}
