﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SimpleCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }

    public class MySimpleCalculator
    {
        public int Add(string userInput)
        {
            int result = 0;
            List<string> delimiters = new List<string>();
            string numbers = userInput;
            
            if(String.IsNullOrEmpty(userInput))
                return -1;

            if (userInput.StartsWith("//"))
            {
                string strDelimiter = userInput.Split('\n')[0];
                var listDelimiters = GetMultipleDelimiters(strDelimiter);
                if (listDelimiters.Count > 0)
                {
                    delimiters.AddRange(listDelimiters);
                }
                else
                {
                    delimiters.Add(strDelimiter.Substring(2));
                }

                numbers = userInput.Split('\n')[1];
            }
            else
            {
                delimiters.Add(",");
                delimiters.Add("\n");
            }

            result = AddNumbers(numbers, delimiters);

            return result;
        }

        private int AddNumbers(string numbersToAdd, List<string> delimiters)
        {
            int result = 0;
            string[] splitNumbers = numbersToAdd.Split(delimiters.ToArray(), StringSplitOptions.None);
            List<string> negativeNumber = new List<string>();

            foreach (var number in splitNumbers)
            {
                int parsedNumber = 0;
                int.TryParse(number, out parsedNumber);

                if (parsedNumber > 0)
                {
                    if (parsedNumber <= 1000)
                        result += parsedNumber;
                }
                else
                {
                    negativeNumber.Add(number);
                }
            }

            if (negativeNumber.Count > 0)
            {
                ThrowException(negativeNumber);
            }

            return result;
        }

        private void ThrowException(List<string> negativeNumber)
        {
            string message =  "[Exception] Negatives NOT allowed!:";
            StringBuilder sb = new StringBuilder(message);
            foreach (var number in negativeNumber)
            {
                sb.Append(number);
                sb.Append(" ");
            }
            throw new Exception(sb.ToString());
        }

        private List<string> GetMultipleDelimiters(string delimiters)
        {
            List<string> result = new List<string>();
            Regex reg = new Regex(@"\[(.)\]");
            var matches = reg.Matches(delimiters);

            foreach (Match delimiter in matches)
            {
                string temp = delimiter.Value.ToString();
                result.Add(temp.Substring(temp.IndexOf("[") + 1, temp.Length - temp.LastIndexOf("]")));
            }

            return result;
        }
    }
}
